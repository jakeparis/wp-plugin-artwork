
// @hat-tip https://bamadesigner.com/2012/03/manage-wordpress-posts-using-bulk-edit-and-quick-edit/#populate_quick_edit
document.addEventListener("DOMContentLoaded", function() {

	// @hat-tip https://plainjs.com/javascript/events/live-binding-event-handlers-14/
	function live(selector, event, callback, context) {
		context = context || document;
		context.addEventListener(event, function(e) {
			var found, el = e.target;
			while (el && !(found = el.id == selector)) el = el.parentElement;
			if (found) callback.call(el, e);
		});
	}

	var wp_inline_edit = window.inlineEditPost.edit;

	window.inlineEditPost.edit = function(id){
		wp_inline_edit.apply(this, arguments);
		if( typeof id !== 'object' )
			return;
		var postId = parseInt( this.getId(id) );
		var editRow = document.getElementById(`edit-${postId}`);

		// price
		editRow.querySelector('input[name="artwork-price"]').value = window.artwork_meta[postId].prices;
	};

	// manage Bulk editing
	// const bulkEdit = document.getElementById('bulk_edit');
	live('bulk_edit', 'click', function(e){
		const bulkEditing = document.getElementById('bulk-edit');
		var postIds = [];
		bulkEditing.querySelectorAll('#bulk-titles div').forEach( el => {
			postIds.push( el.id.replace(/[^0-9]/g,'') );
		});
		var price = bulkEditing.querySelector('input[name="artwork-price"]').value;

		jQuery.post( ajaxurl, {
			action: "artwork_save_bulk_edit",
			post_ids: postIds,
			artworkPrice: price
		});
	});

});