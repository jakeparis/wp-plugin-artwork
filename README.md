# Artwork #

**Contributors:** jakeparis  
**Donate link:** https://jakeparis.com/  
**Tags:** artwork, portfolio  
**Requires at least:** 5.0  
**Tested up to:** 5.9  
**Stable tag:** 1.1.1  
**Requires PHP:** 7.4  
**License:** GPLv2 or later  
**License URI:** https://www.gnu.org/licenses/gpl-2.0.html  

A plugin that sets up artwork as an item type, with the appropriate views and
metadata.

## Description ##

A plugin that sets up artwork as an item type, with the appropriate views and
metadata.

## Changelog ##

### 1.4.0

* added a pricing tool

### 1.3.1

Handle edge case with attachment page redirect

### 1.3.0

Update image attachment page links to point to the artwork record

### 1.2.1 ###

Updated the updater library to 4.11

### 1.2.0 ###

Updated this readme file. Really?

### 1.1.1 ###

Updated updater location to gitlab

### 1.1.0 ###

Compatibility for WP 5.5
