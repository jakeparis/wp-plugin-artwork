<?php 
namespace Jakeparis\Artwork;

defined('ABSPATH') || die('Not allowed');

add_shortcode('artwork-grid', function($atts){
	return getArtworkGrid($atts);
});