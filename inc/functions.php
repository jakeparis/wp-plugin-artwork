<?php
namespace Jakeparis\Artwork;

defined('ABSPATH') || die('Not allowed');

function getArtworkGrid($args=[]){

	$imageSizeDefault = 'thumbnail';
	$args = wp_parse_args( $args, [
		'size' => $imageSizeDefault,
		'tag' => false,
		'year' => false,
		'medium' => false,
		'sort' => 'date',
		'limit' => false,
		'first-large' => false,
	]);

	$queryArgs= [
		'post_type' => 'artwork',
	];

	// put here any wp_query args that are expected as an array
	foreach(['post__not_in','post__in'] as $fieldWantingArray){
		if( ! isset($args[$fieldWantingArray]) )
			continue;
		$queryArgs[$fieldWantingArray] = explode(',', $args[$fieldWantingArray]);
	}

	// don't pass "limit" for no limit. Pass true or "true" to use the admin-set
	// posts-per-page settings. Pass an integer/numberstring to control num of posts
	// displayed.
	if( ! $args['limit'] )
		$queryArgs['nopaging'] = true;
	elseif ( $args['limit'] === true || $args['limit'] === 'true' )
		$queryArgs['paged'] = 1;
	else
		$queryArgs['posts_per_page'] = $args['limit'];

	if( $args['tag'] ){
		if( ! isset($queryArgs['tax_query']) )
			$queryArgs['tax_query'] = array();
		$queryArgs['tax_query'][] = array(
			'taxonomy' => 'post_slug',
			'field' => 'slug',
			'terms' => $args['tag']
		);
	}
	if( $args['medium'] ){
		if( ! isset($queryArgs['tax_query']) )
			$queryArgs['tax_query'] = array();
		$queryArgs['tax_query'][] = array(
			'taxonomy' => 'medium',
			'field' => 'slug',
			'terms' => $args['medium']
		);
	}
	if( $args['year'] ){
		$queryArgs['year'] = $args['year'];
	}

	switch($args['sort']){

		case 'title' :
		case 'name' :
			$queryArgs['order'] = 'ASC';
			$queryArgs['orderby'] = 'title';
			break;

		case 'post__in' :
		case 'manual' :
			$queryArgs['order'] = 'ASC';
			$queryArgs['orderby'] = 'post__in';
			break;
		
		case 'reverse-date' :
			$queryArgs['order'] = 'ASC';
			$queryArgs['orderby'] = 'post_date';
			break;

		case 'random' :
			 // seems odd, but order param is needed for rand to work
			$queryArgs['order'] = 'DESC';
			$queryArgs['orderby'] = 'rand';

		default :
			$queryArgs['order'] = 'DESC';
			$queryArgs['orderby'] = 'post_date';
			break;
	}

	$posts = get_posts( $queryArgs );

	if( ! $posts )
		return false;

	$artworks = array_map(function($post){
		return new Artwork($post);
	}, $posts);

	$imageSize = (in_array($args['size'], ['full','large','medium','thumbnail']))
		? $args['size']
		: $imageSizeDefault;

	$html = '<section class="artwork-grid">';
	foreach($artworks as $key=>$art){
		if( $args['first-large'] !== false && $key==0 )
			$html .= $art->getListViewHtml( 'medium' );
		else
			$html .= $art->getListViewHtml( $imageSize );
	}
	$html .= '</section>';

	return $html;

}

function getAllTags($before="", $sep=", ", $after=""){
	$tags = get_terms([
		'taxonomy' => 'post_tag',
	]);
	if( ! $tags )
		return false;

	$out = [];

	foreach($tags as $tag ){
		$out[] = '<a href="'. get_tag_link($tag->term_id).'">'.$tag->name.'</a>';
	}

	return $before . join($sep, $out) . $after;
}


/** Posts Collections
 */
add_action('init', function(){
	session_start();
});

/* Redirect attachment pages to the artwork for which it is associated (if any exists)
 */
add_action('wp', function(){
	// idea from: https://gschoppe.com/wordpress/disable-attachment-pages/
	if ( ! is_attachment() )
		return;
	
	$attachmentId = get_the_ID();
	$parentId = wp_get_post_parent_id( $attachmentId );
	if( $parentId === 0 )
		return;
	if( $parentId === $attachmentId )
		return;
	$artLink = get_permalink($parentId);
	if( $artLink ) {
		wp_redirect($artLink, 301);
		exit;
	}
});

add_filter('pre_get_posts',function($wp_query){

	if ( empty( $wp_query->query_vars["post_type"] ) || $wp_query->query_vars["post_type"] !== 'artwork' )
		return;

	if( is_admin() || is_single() )
		return;

	/* Block certain tags from regular loops
	 */
	$privateTags = ['just-for-fun', 'sketches', 'detail', 'in-progress'];

	if( isset($wp_query->query_vars['tag'])
		&& ! in_array( $wp_query->query_vars['tag'], $privateTags )
	) {
		$wp_query->set('tax_query', array(
			[
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => $privateTags,
				'operator' => 'NOT IN'
			]
		));
	}

	/* Only non-draft, non-sketch (basically non-children) art
	 */
	if( ! $wp_query->get('post_parent') ){
		$wp_query->set('post_parent', 0);
	}

	/* Store current collection in SESSION var
	 */
	$wp_query->query_vars['suppress_filters'] = false;
	add_filter('the_posts', function( $posts ){

		// if current loop has no posts, unset. We've already checked
		// for single, so we can assume that this is an index loop with only one result
		if( count($posts) < 2){
			$_SESSION['posts-collection'] = false;
			return $posts;
		}

		// if current loop has > 1 post, store post ids
		$_SESSION['posts-collection'] = array_map(function($post){
			return $post->ID;
		}, $posts);

		return $posts;
	});

}, 99);

function getNextPost( $currentId = null, $innerHtml = 'Next &raquo;'){
	if( ! isset($_SESSION['posts-collection']) )
		return get_next_post_link( '%link', $innerHtml );

	if( $_SESSION['posts-collection'] === false )
		return '';

	if(is_null($currentId))
		$currentId = get_the_ID();

	$this_post_collection_key = array_search($currentId, $_SESSION['posts-collection']);

	if( $this_post_collection_key === false )
		return '';

	if( ($this_post_collection_key+1) >= count($_SESSION['posts-collection']) )
		return '';

	$nextPostId = $_SESSION['posts-collection'][($this_post_collection_key+1)];
	$nextPostLink = get_permalink( $nextPostId );
	if( ! $nextPostLink )
		return '';

	return <<<OUT
	<a href="{$nextPostLink}" rel="next" id="adjacent-artwork-link-next">{$innerHtml}</a>
	<script>
	(function(){
		window.addEventListener("keyup",function(e){
			if( e.target.tagName !== 'BODY' )
				return;
			if( e.code === "ArrowRight" ){
				window.location = "{$nextPostLink}";
			}
		});
	})();
	</script>
OUT;

}

function getPrevPost( $currentId = null, $innerHtml = '&laquo; Previous'){
	if( ! isset($_SESSION['posts-collection']) )
		return get_previous_post_link( '%link', $innerHtml );

	if( $_SESSION['posts-collection'] === false )
		return '';

	if(is_null($currentId))
		$currentId = get_the_ID();

	$this_post_collection_key = array_search($currentId, $_SESSION['posts-collection']);

	// false or 0 (first element in array)
	if( ! $this_post_collection_key )
		return '';

	$prevPostId = $_SESSION['posts-collection'][($this_post_collection_key-1)];
	$prevPostLink = get_permalink( $prevPostId );
	if( ! $prevPostLink )
		return '';

	return <<<OUT
	<a href="{$prevPostLink}" rel="previous">{$innerHtml}</a>
	<script>
	(function(){
		window.addEventListener("keyup",function(e){
			if( e.target.tagName !== 'BODY' )
				return;
			if( e.code === "ArrowLeft" ){
				window.location = "{$prevPostLink}";
			}
		});
	})();
	</script>
OUT;

}


add_action( 'admin_menu', function () {
	add_submenu_page( 'tools.php', 'Artwork Tools', 'Artwork', 'edit_posts', 'artwork-tools', __NAMESPACE__ . '\artwork_tools_page' );
});

function artwork_tools_page () {
	require_once ARTWORK_PLUGIN_DIR . 'artwork_tools_admin_page.php';
}