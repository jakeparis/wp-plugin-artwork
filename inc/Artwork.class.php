<?php
namespace Jakeparis\Artwork;

class Artwork {

	/**
	 * Create.
	 *
	 * @param mixed  $mixed Anything that get_post accepts
	 */
	function __construct( $mixed ){
		$post = get_post($mixed);
		if( ! $post )
			return false;

		$this->post = $post;
		$this->loadMeta();
		$this->loadTax();
	}

	private function _getMeta( $key ){
		if( ! isset($this->meta["_$key"]) )
			return false;

		// only single values
		return $this->meta["_$key"][0];
	}
	private function _setMeta( $key, $val ){
		if( ! $val ){

			delete_post_meta( $this->getId(), "_$key" );
			unset( $this->meta[$key] );
		} else {
			update_post_meta( $this->getId(), "_$key", $val );
			$this->meta[$key] = array($val);
		}
	}
	private function loadMeta(){
		$this->meta = get_post_meta($this->getId());
	}
	private function _setPostData( $field, $val, $saveImmediately=false ){
		$this->post->$field = $val;
		if( $saveImmediately ){
			// careful about hooking on save_post actions
			wp_update_post( $this->post );
		}
	}

	private function loadTax(){
		$this->tax = array(
			'medium' => get_the_terms( $this->getId(), 'medium' ),
		);
	}

	function getId() {
		return $this->post->ID;
	}

	/* Post field getters
	 */
	function getTitle(){
		return get_the_title( $this->post );
	}
	function getYear(){
		// return $this->_getMeta('year');
		return date('Y', strtotime( $this->post->post_date) );
	}

	/* Post field setters
	 */
	function setTitle($v, $saveImmediately=false){
		$this->_setPostData('post_title', $v, $saveImmediately);
	}
	function setYear($v, $saveImmediately=false){
		// $this->_setMeta('year', $v);
		// if no month/day in string strtotime() uses today.
		if( strlen($v) == 4 )
			$v = "$v-01-01";
		// will add 000 seconds automatically if not present in string
		$date = date('Y-m-d H:i:s', strtotime($v) );
		$this->_setPostData('post_date', $date, $saveImmediately);
	}
	// Alias
	function setDate($v, $saveImmediately=false){
		return $this->setYear($v,$saveImmediately);
	}


	/* Taxonomy getters
	 */
	function getMedium($before='', $sep = ', ', $after='' ){
		// if( $raw )
		// 	return $this->tax['medium'];
		return get_the_term_list( $this->getId(), 'medium', $before, $sep, $after);
	}
	// function getTags( $sep = ', ', $raw = false ){
	// 	if( $raw )
	// 		return $this->tax['art_tag'];
	// 	$out = [];
	// 	foreach($this->tax['medium'] as $medium){
	// 		$out[ $medium->slug ] = $medium->name;
	// 	}
	// 	return join($sep, $out);
	// }


	/* Meta getters
	 */
	function getSize(){
		return $this->_getMeta('size');
	}
	function getPrice( $prefix = '' ){
		$price = $this->_getMeta('price');
		if( $price ){
			if( ! is_numeric($price) && $prefix == '$' )
				return $price;
			else
				return $prefix . $price;
		}
		return false;
	}

	/* Meta setters
	 */
	function setSize($v){
		$this->_setMeta('size', $v);
	}
	function setPrice($v){
		$this->_setMeta('price', $v);
	}

	function getAllImages(){
		return get_attached_media('image', $this->getId() );
	}

	function getImage($size='full'){
		return get_the_post_thumbnail_url( $this->post, $size );
	}
	function getImageData(){
		$id = get_post_thumbnail_id( $this->post );
		return wp_get_attachment_metadata( $id );
	}
	function getImageOrientation(){
		$data = $this->getImageData();
		if( $data['width'] == $data['height'] )
			return 'square';
		if( $data['width'] > $data['height'] )
			return 'horizontal';
		return 'vertical';
	}

	function getListViewHtml( $imageSize = 'thumbnail' ){
		return '<figure>
			<a href="'.get_the_permalink($this->getId()).'">
			<img src="'.$this->getImage($imageSize).'" alt="'.esc_attr($this->getTitle()).'">
			</a>
			<figcaption>'.$this->getTitle().'</figcaption>
		</figure>';
	}

	function getParent(){
		return $this->post->post_parent;
	}

	function getAssociatedArt( $includingParent = false){
		// get art with me as parent
		$args = [
			'post_type' => 'artwork',
			'post_parent__in' => [$this->getId()],
			'orderby' => ['menu_order'=>'ASC', 'post_date'=>'ASC'],
			'nopaging' => true,
		];
		$posts = get_posts($args);
		if( $posts )
			return $posts;
		if( ! $this->post->post_parent )
			return false;
		// get art siblings with my parent as parent
		$args = array_merge($args, [
			'post_parent__in' => [$this->getParent()],
			'post__not_in' => [$this->getId()],
		]);
		$posts = get_posts($args);
		if( $includingParent )
			$posts = array_merge( $posts, [get_post($this->getParent())] );
		return $posts;
	}

}