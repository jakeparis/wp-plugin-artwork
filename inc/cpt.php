<?php
namespace Jakeparis\Artwork;

defined('ABSPATH') || die('Not allowed');

add_action('init', function(){

	register_post_type( 'artwork', [
		'label' => 'Artwork',
		'labels' => [
			'singular_name' => 'Artwork',
			'not_found' => 'No Art',
			'new_item' => 'New Artwork Record',
			'add_new_item' => 'New Artwork Record',
			'view_item' => 'View Artwork',
			'all_items' => 'All Art',
			'featured_image' => 'Main Image',
			'set_featured_image' => 'Set Main Image',
			'remove_featured_image' => 'Remove Main Image',
			'use_featured_image' => 'Use as main image',
			'edit_item' => 'Edit Artwork Record',
		], 
		'description' => '',
		'public' => true,
		'menu_icon' => 'data:image/svg+xml;base64,' . base64_encode( '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 30"><path d="M21.29,4.29L19.71,2.71a1,1,0,0,0-1.41,0L16,5,8,13l3,3,8-8,2.29-2.29A1,1,0,0,0,21.29,4.29Z" fill="#000000" /><path d="M3.17,15.17A4,4,0,0,0,2,18v4H6A4,4,0,1,0,3.17,15.17Z" fill="#000000" /></svg>'),
		'supports' => [
			'title',
			'thumbnail',
			'excerpt',
			'page-attributes'
			// 'comments',
		],
		'hierarchical' => true,
		'taxonomies' => ['post_tag','medium'],
		'rewrite' => [
			'with_front' => false,
			'feeds' => true
		],
		'has_archive' => true,
		'show_in_rest' => true,
	] );

	register_taxonomy( 'medium', ['artwork'], [
		'label' => 'Medium',
		'labels' => [
			'popular_items' => 'Select from commonly used media',
			'choose_from_most_used' => 'Select from commonly used media',
			'not_found' => 'No media listed',
			'search_items' => 'Search for media',
			'separate_items_with_commas' => 'Separate with commas',
		],
		'show_in_rest' => true,
		'show_admin_column' => true,
	]);

});

add_action('add_meta_boxes_artwork', function(){
	add_meta_box( 'art-details', 'Art Details', __NAMESPACE__.'\details_metacallback', ['artwork'], 'normal', 'high' );
});
function details_metacallback( $post ){
	$art = new Artwork($post);

	wp_nonce_field( 'save_artwork_settings', '_artwork_settings_nonce' );
	echo '<label class="block" for="artwork-size">Size</label>
		<input type="text" name="artwork-size" id="artwork-size" value="'.esc_attr($art->getSize()).'">
		<label class="block" for="artwork-price">Price</label>
			$<input type="text" name="artwork-price" id="artwork-price" value="'.esc_attr($art->getPrice()).'">
		';
}

add_action('save_post_artwork',function($post_id){

	if( ! wp_verify_nonce( $_POST['_artwork_settings_nonce'], 'save_artwork_settings' ) &&
		! defined('DOING_AJAX')
	) {
		return;
	}

	if( defined('DOING_AUTOSAVE') )
		return;

	if( ! current_user_can( 'edit_post', $post_id ) )
		return;

	$art = new Artwork($post_id);
	$art->setSize( $_POST['artwork-size'] );
	$art->setPrice( $_POST['artwork-price'] );

});


add_filter('manage_artwork_posts_columns', function($cols){

	$cols['price'] = 'Price';

	// doing it this way so Image will be first.
	$new = [
		'image' => 'Image',
	];
	$cols = array_merge($new, $cols);

	return $cols;
});
add_filter('manage_artwork_posts_custom_column', function($col, $post_id){
	switch($col) {
		case 'image':
			$art = new Artwork($post_id);
			echo '<img src="'.$art->getImage('thumbnail').'" style="height: 125px;">';
			break;
		case 'price':
			$art = new Artwork($post_id);
			$p = $art->getPrice();
			echo '<script>
			artwork_meta = window.artwork_meta || [];
			if( typeof artwork_meta["'.$post_id.'"] == "undefined")
				artwork_meta["'.$post_id.'"] = {};
			artwork_meta["'.$post_id.'"].prices = "'.$p.'";
			</script>';
			if( is_numeric($p) )
				echo '$' . $p;
			else
				echo $p;
			break;
	}
},10,2);

add_filter('manage_edit-artwork_sortable_columns', function($cols){
	$cols['price'] = 'price';
	return $cols;
});
add_filter('request', function($vars){
	if( ! is_admin() )
		return $vars;
	if( $vars['post_type'] !== 'artwork' )
		return $vars;
	if( $vars['orderby'] == 'price' )
		$vars = array_merge($vars, [
			'meta_key' => '_price',
			'orderby' => 'meta_value_num',
		]);

	return $vars;
});

add_action('quick_edit_custom_box',__NAMESPACE__.'\quickAndBulkEditBox',10,2);
add_action('bulk_edit_custom_box',__NAMESPACE__.'\quickAndBulkEditBox',10,2);
function quickAndBulkEditBox($col_name,$post_type){
	if( $post_type !== 'artwork' )
		return;

	switch($col_name) {
		case 'price' :
			echo '
			<fieldset class="inline-edit-col-right">
				<div class="inline-edit-col">
					<label>
						<span class="title">Price</span>
						<span class="input-text-wrap">
							<input class="inline-edit-artwork-price-input" type="text"
								name="artwork-price" value="">
						</span>
					</label>
				</div>
			</fieldset>';
			break;
	}
}

// bulk editing
add_action('wp_ajax_artwork_save_bulk_edit',function(){
	$postIds = $_POST['post_ids'];
	$price = $_POST['artworkPrice'];
	// Note: we don't allow setting an empty price from
	// bulk. Because otherwise "not" setting the field 
	// would clear it!
	if( ! $postIds || ! $price )
		return;

	$artworks = array_map(function($id){
		return new Artwork($id);
	}, $postIds);

	foreach($artworks as $art){
		$art->setPrice($price);
	}
});