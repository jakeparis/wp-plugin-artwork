<?php
namespace Jakeparis\Artwork;
/*
Plugin Name: Artwork
Plugin URI:  https://gitlab.com/jake.paris/plugin-artwork/
Description: Provides for an Artwork post type
Version:     1.4.0
Author:      Jake Paris
Author URI:  https://jakeparis.com/
License:     MIT
License URI: https://opensource.org/licenses/MIT
*/

define('ARTWORK_PLUGIN_VERSION', '1.4.0' );
define('ARTWORK_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define('ARTWORK_URL', plugins_url('/', __FILE__ ));

foreach(glob( ARTWORK_PLUGIN_DIR . 'inc/*.php') as $file ){
	require_once $file;
}

add_action('admin_enqueue_scripts',function(){

	wp_enqueue_script( 'artwork-admin', 
		ARTWORK_URL.'assets/artwork-admin.js', 
		['inline-edit-post'], '1.0.0' );

});

add_action('admin_enqueue_scripts', function(){

	wp_enqueue_style( 'artwork-admin', ARTWORK_URL.'assets/artwork-admin.css', [], '1.0.0' );

});



/**
 * Updater
 */
require plugin_dir_path(__FILE__) . 'updater/plugin-update-checker.php';
$myUpdateChecker = \Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/wp-plugin-artwork/',
	__FILE__, //Full path to the main plugin file or functions.php.
	'artwork/artwork.php'
);


