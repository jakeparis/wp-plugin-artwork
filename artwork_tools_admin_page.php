<?php
defined( 'ABSPATH' ) || exit;
?>
<style>
.pricing-form {
	display: flex;
	gap: .75em;
	border: 1px solid gray;
	border-radius: 3px;
	margin: 1em 0;
	padding: 1em;

	i {
		display: block;
	}
	label {
		font-weight: bold;
		display: block;
	}
}
#price {
	background: yellowgreen;
	display: inline-block;
	padding: 4px;
}

</style>
<div class="wrap">
	<h1>Artwork Tools</h1>

	<p>Enter the artwork size and quality and get a price output: <pre id="formula">base + (h*w*Q*ppi) = Price</pre></p>

	<div class="pricing-form">
		<div>
			<label>Height</label>
			<i>Inches</i>
			<input type="number" id="height" size="4">
		</div>
		<div>
			<label>Width</label>
			<i>Inches</i>
			<input type="number" id="width" size="4">
		</div>
		<div>
			<label>Quality Factor</label>
			<i>Raise or lower to denote different quality</i>
			- <input type="range" min=".2" max="2" step=".1" id="quality"> + 
			<br>
			<span id="_quality">1</span>
		</div>
		<div>
			<label>Price/inch</label>
			<i>Usually doesn't change</i>
			<input type="text" id="ppi" value="2.5" size="4">
		</div>
		<div>
			<label>Base price</label>
			<i>Usually doesn't change</i>
			<input type="text" id="base" value="150" size="4">
		</div>
	</div>

	<button id="calc-price" class="button button-primary">Calculate</button>

	<h3>Price: <span id="price">0</span></h3>

	<script>
		document.addEventListener('DOMContentLoaded', function(){
			var h = document.getElementById('height');
			var w = document.getElementById('width');
			var q = document.getElementById('quality');
			var _q = document.getElementById('_quality');
			var ppi = document.getElementById('ppi');
			var base = document.getElementById('base');
			var calc = document.getElementById('calc-price');
			var price = document.getElementById('price');


			q.addEventListener('change', e => {
				_q.innerText = e.target.value;
			});

			calc.addEventListener( 'click', e => {
				e.preventDefault();
				if ( h.value == '' || w.value == '' )
					return;
				var p = parseFloat( base.value ) + parseFloat(height.value) * parseFloat(width.value)
					* parseFloat( q.value ) * parseFloat( ppi.value );
				p = Math.round( p );
				price.innerText = `$${p}`;
			});
			
		});
	</script>

	<!-- <form method="post" action="">

		<?php submit_button(); ?>
	</form> -->
</div>